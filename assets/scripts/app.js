const productList = {
    products:[
        { title: 'A Pillow', imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51Cmyq2dlTL._AC_SL1500_.jpg', price: 19.99, description: 'A soft pillow!' },  
        { title: 'A Carpet', imageUrl: 'https://cf.shopee.com.my/file/7a2ee3347f0055933b1e43ef13a71004', price: 86.99, description: 'A cheap carpet you might like!' },  
    ],
    render() {
        const renderHook = document.getElementById('app');
        const prodList = document.createElement('ul');
        prodList.classList.add('product-list');
        for (const prod of this.products) {
            const prodEl = document.createElement('li');
            prodEl.classList.add('product-item');
            prodEl.innerHTML = `
                <div>
                    <img src='${prod.imageUrl}' alt='${prod.title}'/>
                    <div class='product-item__content'>
                        <h2>${prod.title}</h2>
                        <h3>\$${prod.price}</h3>
                        <p>${prod.description}</p>
                        <button>Add to Cart</button>
                    </div>
                </div>
            `
            prodList.append(prodEl);
        }
        renderHook.append(prodList);
    }
};

productList.render();